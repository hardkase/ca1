ProjectName = simulate
CC = g++
DEPS = Job.hpp scripto
OBJ = main.o Job.o

scripto :
	ca1_jobs.pl 10 40 20 > ca1_jobs.txt
%.o: %.cpp $(DEPS)
	$(CC) -c -o $@ $^ -I.
$(ProjectName): $(OBJ)
	$(CC) -o $@ $^ -I.
