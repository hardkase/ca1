#pragma once
#include <string>

class Job
{
public:
	Job(std::string name, unsigned int arrival, unsigned int duration, unsigned int timeRemaining, unsigned inttAt0, unsigned intrt0,
		unsigned int tAt1, unsigned int rt1, unsigned int tAt2, unsigned int rt2, unsigned int tAt3, unsigned int rt3);
	Job(std::string name, unsigned int arrival, unsigned int duration);
	Job();
	bool operator < (const Job &other) const
	{
		return other.arrival < arrival;
	}
	std::string getName() const;
	unsigned int getArrival() const;
	unsigned int getDuration() const;
	unsigned int getTimeRemaining() const;
	unsigned int getTaT0() const;
	unsigned int getTaT1() const;
	unsigned int getTaT2() const;
	unsigned int getTaT3() const;
	unsigned int getRt0() const;
	unsigned int getRt1() const;
	unsigned int getRt2() const;
	unsigned int getRt3() const;
	void setName(std::string name);
	void setArrival(unsigned int arrival);
	void setDuration(unsigned int duration);
	void setTimeRemaining(unsigned int timeRemaining);
	void setTaT0(unsigned int tAt0);
	void setTaT1(unsigned int tAt1);
	void setTaT2(unsigned int tAt2);
	void setTaT3(unsigned int tAt3);
	void setRt0(unsigned int rt0);
	void setRt1(unsigned int rt1);
	void setRt2(unsigned int rt2);
	void setRt3(unsigned int rt3);
	void print();
private:
	std::string name;
	unsigned int arrival, duration, timeRemaining, tAt0, rt0, tAt1, rt1, tAt2, rt2, tAt3, rt3;
};