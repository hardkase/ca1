#include "Job.hpp"
#include <iostream>
#include <string>

Job::Job(std::string name, unsigned int arrival, unsigned int duration, unsigned int timeRemaining, unsigned inttAt0, unsigned intrt0,
	unsigned int tAt1, unsigned int rt1, unsigned int tAt2, unsigned int rt2, unsigned int tAt3, unsigned int rt3) :name(name), arrival(arrival), 
	duration(duration), timeRemaining(0), tAt0(0), rt0(0), tAt1(0), rt1(0), tAt2(0), rt2(0), tAt3(0), rt3(0)
{

}
Job::Job(std::string name, unsigned int arrival, unsigned int duration) : name(name), arrival(arrival), duration(duration)
{
	setTimeRemaining(0);
	setTaT0(0);
	setTaT1(0);
	setTaT2(0);
	setTaT3(0);
	setRt0(0);
	setRt1(0);
	setRt2(0);
	setRt3(0);
}
Job::Job()
{

}
std::string Job::getName() const
{
	return name;
}
unsigned int Job::getArrival() const
{
	return arrival;
}
unsigned int Job::getDuration() const
{
	return duration;
}
unsigned int Job::getTimeRemaining() const
{
	return timeRemaining;
}
unsigned int Job::getTaT0() const
{
	return tAt0;
}
unsigned int Job::getTaT1() const
{
	return tAt1;
}
unsigned int Job::getTaT2() const
{
	return tAt2;
}
unsigned int Job::getTaT3() const
{
	return tAt3;
}
unsigned int Job::getRt0() const
{
	return rt0;
}
unsigned int Job::getRt1() const
{
	return rt1;
}
unsigned int Job::getRt2() const
{
	return rt2;
}
unsigned int Job::getRt3() const
{
	return rt3;
}
void Job::setName(std::string name)
{
	name = name;
}
void Job::setArrival(unsigned int arrival)
{
	arrival = arrival;
}
void Job::setDuration(unsigned int duration)
{
	duration = duration;
}
void Job::setTimeRemaining(unsigned int timeRemaining)
{
	timeRemaining = timeRemaining;
}
void Job::setTaT0(unsigned int tAt0)
{
	tAt0 = tAt0;
}
void Job::setTaT1(unsigned int tAt1)
{
	tAt0 = tAt1;
}
void Job::setTaT2(unsigned int tAt0)
{
	tAt2 = tAt2;
}
void Job::setTaT3(unsigned int tAt0)
{
	tAt0 = tAt3;
}
void Job::setRt0(unsigned int rt0)
{
	rt0 = rt0;
}
void Job::setRt1(unsigned int rt0)
{
	rt1 = rt1;
}
void Job::setRt2(unsigned int rt2)
{
	rt2 = rt2;
}
void Job::setRt3(unsigned int rt3)
{
	rt3 = rt3;
}
void Job::print()
{
	std::cout << "Name: " << getName() << "\n" << std::endl;
	std::cout << "Job Arrival Time: " << getArrival() << "\n" << std::endl;
	std::cout << "Job Duration: " << getDuration() << "\n" << std::endl;
	std::cout << "Time Remaining: " << getTimeRemaining() << std::endl;
}